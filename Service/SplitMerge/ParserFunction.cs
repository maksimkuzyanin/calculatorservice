﻿using System;
using System.Collections.Generic;

namespace SplitMerge
{
    public class ParserFunction
    {
        private double evaluatedValue;

        public ParserFunction()
        {
            m_impl = this;
        }

        public ParserFunction(double evaluatedValue): this()
        {
            this.evaluatedValue = evaluatedValue;
        }

        // A "virtual" Constructor
        internal ParserFunction(string data, ref int from, string item, char ch)
        {
            if (item.Length == 0 && ch == Parser.START_ARG)
            {
                // There is no function, just an expression in parentheses
                m_impl = s_idFunction;
                return;
            }

            if (m_functions.TryGetValue(item, out m_impl))
            {
                // Function exists and is registered (e.g. pi, exp, etc.)
                return;
            }

            // Function not found, will try to parse this as a number.
            s_strtodFunction.Item = item;
            m_impl = s_strtodFunction;
        }

        public static void addFunction(string name, ParserFunction function)
        {
            m_functions[name] = function;
        }

        public double getValue(string data, ref int from)
        {
            return m_impl.evaluate(data, ref from);
        }

        protected virtual double evaluate(string data, ref int from)
        {
            // The real implementation will be in the derived classes.
            return evaluatedValue;
        }

        private ParserFunction m_impl;
        private static Dictionary<string, ParserFunction> m_functions = new Dictionary<string, ParserFunction>();

        private static StrtodFunction s_strtodFunction = new StrtodFunction();
        private static IdentityFunction s_idFunction = new IdentityFunction();
    }

    public class StrtodFunction : ParserFunction
    {
        protected override double evaluate(string data, ref int from)
        {
            double num;
            if (!Double.TryParse(Item, out num))
            {
                throw new ArgumentException("Could not parse token [" + Item + "]");
            }
            return num;
        }
        public string Item { private get; set; }
    }

    public class IdentityFunction : ParserFunction
    {
        protected override double evaluate(string data, ref int from)
        {
            return Parser.loadAndCalculate(data, ref from, Parser.END_ARG);
        }
    }

    public class PiFunction : ParserFunction
    {
        protected override double evaluate(string data, ref int from)
        {
            return 3.141592653589793;
        }
    }
    public class ExpFunction : ParserFunction
    {
        protected override double evaluate(string data, ref int from)
        {
            double arg = Parser.loadAndCalculate(data, ref from, Parser.END_ARG);
            return Math.Exp(arg);
        }
    }
    public class PowFunction : ParserFunction
    {
        protected override double evaluate(string data, ref int from)
        {
            double arg1 = Parser.loadAndCalculate(data, ref from, ',');
            double arg2 = Parser.loadAndCalculate(data, ref from, Parser.END_ARG);

            return Math.Pow(arg1, arg2);
        }
    }
    public class SinFunction : ParserFunction
    {
        protected override double evaluate(string data, ref int from)
        {
            double arg = Parser.loadAndCalculate(data, ref from, Parser.END_ARG);
            return Math.Sin(arg);
        }
    }
    public class SqrtFunction : ParserFunction
    {
        protected override double evaluate(string data, ref int from)
        {
            double arg = Parser.loadAndCalculate(data, ref from, Parser.END_ARG);
            return Math.Sqrt(arg);
        }
    }
    public class AbsFunction : ParserFunction
    {
        protected override double evaluate(string data, ref int from)
        {
            double arg = Parser.loadAndCalculate(data, ref from, Parser.END_ARG);
            return Math.Abs(arg);
        }
    }
}
