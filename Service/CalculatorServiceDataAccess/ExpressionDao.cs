﻿using CalculatorDataBaseSample.ObjectModel;
using Microsoft.ServiceModel.Samples.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorServiceDataAccess
{
    /// <summary>
    /// 
    /// </summary>
    public class ExpressionDao
    {
        /// <summary>
        /// Контекст
        /// </summary>
        private CalculatorServiceContext dbContext;

        public ExpressionDao()
        {
            dbContext = new CalculatorServiceContext();
        }

        public Expression GetExpressionById(int id)
        {
            return dbContext.Expressions.Where(x => x.ExpressionId == id).FirstOrDefault();
        }

        public Expression GetExpressionByName(string name)
        {
            return dbContext.Expressions.Where(x => x.ExpressionName == name).FirstOrDefault();
        }

        public bool IsExistsExpression(string name)
        {
            return GetExpressionByName(name) == null;
        }

        public int AddExpression(string name, string formulaExpression, string value = null)
        {
            Expression expression = new Expression {
                ExpressionName = name,
                FormulaExpression = formulaExpression,
                ExpressionValue = value
            };

            dbContext.Expressions.Add(expression);
            dbContext.SaveChanges();

            return expression.ExpressionId;
        }

        public void UpdateExpression(int id, string value)
        {
            var expression = GetExpressionById(id);

            if (expression == null)
            {
                return;
            }
            
            expression.ExpressionValue = value;
            dbContext.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Expression> GetAll()
        {
            return dbContext.Expressions.ToArray();
        }

        public IEnumerable<ExpressionDto> GetAllDtos()
        {
            return GetAll().Select(MapExpression);
        }

        public IEnumerable<ExpressionDto> GetExpressionsByOperandName(string name)
        {
            return dbContext.Expressions
                .Where(x => x.FormulaExpression.Contains(name))
                .Select(MapExpression);
        }

        public string GetExpressionValue(string name)
        {
            Expression expression = GetExpressionByName(name);

            if (expression == null)
            {
                return null;
            }

            return expression.ExpressionValue;
        }


        #region Private Methods

        private ExpressionDto MapExpression(Expression expression)
        {
            return new ExpressionDto
            {
                Id = expression.ExpressionId,
                Name = expression.ExpressionName,
                FormulaExpression = expression.FormulaExpression,
                Value = expression.ExpressionValue
            };
        }

        #endregion
    }
}
