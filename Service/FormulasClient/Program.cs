﻿using FormulasClient.CreateFormulaClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulasClient
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculatorClient createFormulaClient = new CalculatorClient();

            Console.WriteLine("--- Клиент для ввода формул ---");

            string line;

            do {
                Console.WriteLine("Введите формулу в формате <имя> = <выражение>, например C = (A - B) * 4 + 23: ");
                line = Console.ReadLine();

                createFormula(createFormulaClient, line);

            } while(line != null);
        }

        private static void createFormula(CalculatorClient createFormulaClient, string expression)
        {
            try
            {
                createFormulaClient.CreateFormula(expression);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
