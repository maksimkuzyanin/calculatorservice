﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SplitMerge;
using Microsoft.ServiceModel.Samples;
using CalculatorServiceDataAccess;

namespace CalculatorServiceUnitTests
{
    [TestClass]
    public class CalculatorServiceUnitTests
    {

        [TestMethod]
        public void CreateFormulaTest()
        {
            CalculatorService service = new CalculatorService();
            ExpressionDao dao = new ExpressionDao();

            string formulaA = "A = (B + 2) * 3";
            service.CreateFormula(formulaA);

            string formulaB = "B = 3";
            service.CreateFormula(formulaB);

            bool formulaAExists = dao.IsExistsExpression("A");
            bool formulaBExists = dao.IsExistsExpression("B");

            Assert.IsTrue(formulaAExists);
            Assert.IsTrue(formulaBExists);
        }

        [TestMethod]
        public void GetResultTest()
        {
            CalculatorService service = new CalculatorService();
            ExpressionDao dao = new ExpressionDao();

            string formulaA = "A = (B + 2) * 3";
            service.CreateFormula(formulaA);

            string formulaB = "B = 3";
            service.CreateFormula(formulaB);

            string formulaAResult = service.GetResult("A");
            string formulaBResult = service.GetResult("B");

            Assert.AreEqual("15", formulaAResult);
            Assert.AreEqual("3", formulaBResult);
        }
    }
}
