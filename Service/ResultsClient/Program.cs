﻿using ResultsClient.GetResultsClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResultsClient
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculatorClient getResultsClient = new CalculatorClient();

            Console.WriteLine("--- Клиент для ввода формул ---");

            string line;

            do
            {
                Console.WriteLine("Введите формулу в формате <имя> = <выражение>, например C = (A - B) * 4 + 23: ");
                line = Console.ReadLine();

                getResult(getResultsClient, line);

            } while (line != null);
        }

        private static void getResult(CalculatorClient getResultsClient, string name)
        {
            try
            {
                string result;
                do
                {
                    result = getResultsClient.GetResult(name);
                } while (result != null);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
