﻿using CalculatorServiceDataAccess;
using Microsoft.ServiceModel.Samples.DTO;
using SplitMerge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Microsoft.ServiceModel.Samples
{
    public class CalculatorService: ICalculator
    {
        /// <summary>
        /// 
        /// </summary>
        private ExpressionDao dao;

        public CalculatorService()
        {
            dao = new ExpressionDao();
            // загрузка корректных формул из базы
            LoadParserFunctions();
        }

        public void CreateFormula(string expression)
        {
            // 
            ExpressionDto dto = ParseExpression(expression);
            if(dao.IsExistsExpression(dto.Name))
            {
                throw new Exception(Messages.ErrorExistsFormula);
            }

            //
            int expressionId = dao.AddExpression(dto.Name, dto.FormulaExpression, dto.Value);
            dto.Id = expressionId;

            string succOrErrorResult = ProcessExpr(dto);
            ReCalculate(dto.Name, succOrErrorResult);
        }

        public string GetResult(string formulaName)
        {
            if (!dao.IsExistsExpression(formulaName))
            {
                throw new Exception(Messages.ErrorNotExistsFormula);
            }

            return dao.GetExpressionValue(formulaName);
        }

        #region Private Methods

        /// <summary>
        /// Разбирает выражение
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private ExpressionDto ParseExpression(string expression)
        {
            expression = expression.Replace(" ", string.Empty);

            string[] exprParts = expression.Split('=');
            if (exprParts.Length != 2)
            {
                throw new Exception(Messages.ErrorValidExpression);
            }

            string name = exprParts[0];
            if(!checkOperand(name, false))
            {
                throw new Exception(Messages.ErrorValidExpressionName);
            }

            string formulaExpression = exprParts[1];
            if (!checkFormulaExpression(formulaExpression)) {
                throw new Exception(Messages.ErrorValidFormulaExpression);
            }

            ExpressionDto dto = new ExpressionDto
            {
                Name = name,
                FormulaExpression = formulaExpression
            };

            return dto;
        }

        private bool checkFormulaExpression(string formulaExpression)
        {
            string[] operands = formulaExpression.Split(new[] { '+', '-', '*', '/', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);
            foreach(var operand in operands) {
                if (!checkOperand(operand)) {
                    return false;
                }
            }

            return true;
        }

        private bool checkOperand(string operand, bool isOnlyDigits = true)
        {
            string lettersAndDigits = "^[A-Za-z0-9]+$";
            string onlyLetters = "[A-Za-z]";
            string onlyDigits = "[0-9]";

            if (Regex.IsMatch(operand, lettersAndDigits) ||
                Regex.IsMatch(operand, onlyLetters) ||
                (isOnlyDigits && Regex.IsMatch(operand, onlyDigits)))
            {
                return true;
            }

            return false;
        }

        private void LoadParserFunctions()
        {
            var expressions = dao.GetAllDtos();

            foreach(var expression in expressions)
            {
                double evaluatedValue;
                if (double.TryParse(expression.Value, out evaluatedValue))
                {
                    AddParserFunction(expression.Name, evaluatedValue);
                }
            }
        }

        private void AddParserFunction(string funcName, double evaluatedValue)
        {
            ParserFunction.addFunction(funcName, new ParserFunction(evaluatedValue));
        }

        private string ProcessExpr(ExpressionDto dto)
        {
            string succOrErrorResult;
            try
            {
                double result = Parser.process(dto.FormulaExpression);
                AddParserFunction(dto.Name, result);

                succOrErrorResult = result.ToString();
            }
            catch (Exception e)
            {
                succOrErrorResult = e.Message;
            }

            dao.UpdateExpression(dto.Id, succOrErrorResult);

            return succOrErrorResult;
        }

        private void ReCalculate(string name, string succOrErrorValue)
        {
            double value;
            if (!double.TryParse(succOrErrorValue, out value))
            {
                return;
            }

            var expressions = dao.GetExpressionsByOperandName(name);
            foreach (var expression in expressions)
            {
                ProcessExpr(expression);
            }
        }

        #endregion
    }
}
