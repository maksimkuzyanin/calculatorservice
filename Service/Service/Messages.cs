﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.ServiceModel.Samples
{
    /// <summary>
    /// 
    /// </summary>
    public class Messages
    {
        public const string ErrorExistsFormula = "Формула с таким названием уже существует";
        public const string ErrorValidExpression = "Введена некорректная формула. Верный формат A = <выражение>";
        public const string ErrorValidExpressionName = "Введено некорректное название формулы. Формат названия <Буква><Цифры или буквы>";
        public const string ErrorValidFormulaExpression = "Введено некорректное выражение";
        public const string ErrorNotExistsFormula = "Запрошенная формула не существует";
    }
}
