﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.ServiceModel.Samples
{
    [ServiceContract(Namespace = "http://Microsoft.ServiceModel.Samples")]
    public interface ICalculator
    {
        /// <summary>
        /// Создает формулу
        /// </summary>
        /// <param name="expression">Разбираемое строковое выражение</param>
        /// <returns>Возвращает true - формула создана, false - формула не создана</returns>
        [OperationContract]
        void CreateFormula(string expression);

        /// <summary>
        /// Получает результат вычисления формулы
        /// </summary>
        /// <param name="formulaName">Название формулы</param>
        /// <returns>Возвращает строковый результат вычисления формулы</returns>
        [OperationContract]
        string GetResult(string formulaName);
    }
}
